# Typer

## Example
```console
$ cargo run -- examples/test.ty
```

## Dependencies
### xdo
Debian
```console
$ apt-get install libxdo-dev
```
On Arch:
```console
$ pacman -S xdotool
```
On Fedora:
```console
$ dnf install libX11-devel libxdo-devel
```
On Gentoo:
```console
$ emerge -a xdotool
```
