#!/bin/bash
set -xe

cargo build --release
cp -p target/release/typer ~/.local/bin/typer
