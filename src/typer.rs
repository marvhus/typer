use crate::parse::{
	Commands,
	Command
};
use enigo::*;
use std::{
	thread::sleep,
	time::Duration,
};

pub enum KeybindPreset {
    Emacs,
    Vim,
}

fn key_comb(enigo: &mut Enigo, m: Key, c: char) {
	enigo.key_down(m);
	enigo.key_click(Key::Layout(c));
	enigo.key_up(m);
}

fn write(enigo: &mut Enigo, t: &String, preset: &KeybindPreset, after: bool) {
	enigo.set_delay(200000);
    match preset {
        KeybindPreset::Emacs => {
            enigo.key_sequence(t.as_str());
        },
        KeybindPreset::Vim => {
            enigo.key_click(Key::Layout(if after {'a'} else {'i'}));
            enigo.key_sequence(t.as_str());
            enigo.key_click(Key::Escape);
        }
    }
	enigo.set_delay(50000);
}

pub fn typer(commands: Commands, preset: KeybindPreset) {
	let mut enigo = Enigo::new();
	enigo.set_delay(50000);
	for command in commands.iter() {
		match command {
			Command::GoTo(n) => {
				println!("=> Go to line {}", n);
                match preset {
				    KeybindPreset::Emacs => {
                        key_comb(&mut enigo, Key::Alt, 'g');
				        key_comb(&mut enigo, Key::Alt, 'g');
                        enigo.key_sequence(n.as_str());
                        enigo.key_click(Key::Return);
                    },
                    KeybindPreset::Vim => {
                        enigo.key_click(Key::Layout(':'));
                        enigo.key_sequence(n.as_str());
                        enigo.key_click(Key::Return);
                    }
                }
			},
			Command::MoveUp(n) => {
				println!("=> Move {} lines up", n);
                match preset {
                    KeybindPreset::Vim => {
                        enigo.key_sequence(format!("{}k", n).as_str());
                    },
				    KeybindPreset::Emacs => {
                        for _ in 0..(*n) {
					        enigo.key_click(Key::UpArrow);
                        }
                    },
                }
			},
			Command::MoveDown(n) => {
				println!("=> Move {} lines down", n);
                match preset {
                    KeybindPreset::Vim => {
                        enigo.key_sequence(format!("{}j", n).as_str());
                    },
				    KeybindPreset::Emacs => {
                        for _ in 0..(*n) {
					        enigo.key_click(Key::DownArrow);
                        }
                    },
                }
			},
            Command::MoveLeft(n) => {
                println!("=> Move {} left", n);
                match preset {
                    KeybindPreset::Vim => {
                        enigo.key_sequence(format!("{}l", n).as_str());
                    },
				    KeybindPreset::Emacs => {
                        for _ in 0..(*n) {
					        enigo.key_click(Key::LeftArrow);
                        }
                    },
                }
            },
            Command::MoveRight(n) => {
                println!("=> Move {} left", n);
                match preset {
                    KeybindPreset::Vim => {
                        enigo.key_sequence(format!("{}h", n).as_str());
                    },
				    KeybindPreset::Emacs => {
                        for _ in 0..(*n) {
					        enigo.key_click(Key::LeftArrow);
                        }
                    },
                }
            },
            Command::FindCharRight(c) => {
                println!("=> Find '{}' to the right", c);
                match preset {
                    KeybindPreset::Vim => {
                        enigo.key_click(Key::Layout('f'));
                        enigo.key_click(Key::Layout(*c));
                    },
                    KeybindPreset::Emacs => {
                        unimplemented!("FindCharRight is currently unimplemented for Emacs")
                    },
                }
            },
            Command::FindCharLeft(c) => {
                println!("=> Find '{}' to the left", c);
                match preset {
                    KeybindPreset::Vim => {
                        enigo.key_click(Key::Layout('F'));
                        enigo.key_click(Key::Layout(*c));
                    },
                    KeybindPreset::Emacs => {
                        unimplemented!("FindCharLeft is currently unimplemented for Emacs")
                    },
                }
            },
			Command::AddLine(t) => {
				println!("=> Add Line '{}'", t);
				enigo.key_click(Key::End);
                match preset {
                    KeybindPreset::Emacs => {
                        enigo.key_click(Key::Return);
                        write(&mut enigo, t, &preset, false);
                    },
                    KeybindPreset::Vim => {
                        enigo.key_click(Key::Layout('o'));
                        enigo.key_click(Key::Escape);
                        write(&mut enigo, t, &preset, false);
                    }
                }
			},
			Command::Write(t) => {
				println!("=> Write '{}'", t);
				write(&mut enigo, t, &preset, true);
			},
            Command::YankLine => {
                println!("=> Yank Line");
                match preset {
                    KeybindPreset::Vim => {
                        enigo.key_sequence("yy");
                    },
                    KeybindPreset::Emacs => {
                        // go to beginning of line
                        key_comb(&mut enigo, Key::Control, 'a');
                        // start selection
                        key_comb(&mut enigo, Key::Control, ' ');
                        // go to end of line
                        key_comb(&mut enigo, Key::Control, 'e');
                        // copy selection
                        key_comb(&mut enigo, Key::Alt, 'w');
                    },
                }
            },
            Command::PaseLine(count) => {
                println!("=> Pase Line {} times", count);
                for _ in 0..(*count) {
                    match preset {
                        KeybindPreset::Vim => {
                            enigo.key_click(Key::Layout('p'));
                        },
                        KeybindPreset::Emacs => {
                            key_comb(&mut enigo, Key::Control, 'e');
                            key_comb(&mut enigo, Key::Control, 'o');
                            enigo.key_click(Key::DownArrow);
                            key_comb(&mut enigo, Key::Control, 'a');
                            key_comb(&mut enigo, Key::Control, 'y');
                        },
                    }
                }
            },
			Command::Prepend(t) => {
				println!("=> Prepend '{}'", t);
                enigo.key_click(Key::Home);
                write(&mut enigo, t, &preset, false);
			},
			Command::Append(t) => {
				println!("=> Append '{}'", t);
                enigo.key_click(Key::End);
                write(&mut enigo, t, &preset, true);
			},
			Command::RemoveLines(n) => {
				println!("=> Remove {} lines", n);
				for _ in 0..(*n) {
                    match preset {
                        KeybindPreset::Emacs => {
                            key_comb(&mut enigo, Key::Control, 'a');
                            key_comb(&mut enigo, Key::Control, 'k');
                            key_comb(&mut enigo, Key::Control, 'k');
                        },
                        KeybindPreset::Vim => {
                            enigo.key_sequence("dd");
                        }
                    }
				}
			},
            Command::DeleteInner(c) => {
                println!("=> Deleting inner {}", c);
                match preset {
                    KeybindPreset::Vim => {
                        enigo.key_sequence(format!("ci{}", c).as_str());
                        enigo.key_click(Key::Escape);
                    },
                    KeybindPreset::Emacs => {
                        unimplemented!("DeleteInner unimplemented for Emacs");
                    }
                }
            },
			Command::SleepMs(ms) => {
				println!("=> Sleeping {} ms", ms);
				sleep(Duration::from_millis(*ms));
			}
		}
		sleep(Duration::from_millis(100));
	}
}
