use std::vec::Vec;

#[derive(Debug)]
pub enum Command {
	GoTo(String),

	MoveUp(usize),
	MoveDown(usize),
    MoveLeft(usize),
    MoveRight(usize),

    FindCharRight(char),
    FindCharLeft(char),
	
	AddLine(String),
	Write(String),

    YankLine,
    PaseLine(usize), // count
    
	Prepend(String),
	Append(String),

	RemoveLines(usize),
    DeleteInner(char), // w for word, { for everything inside {}, etc
	
	SleepMs(u64), 
}
pub type Commands = Vec<Command>;

pub fn parse(text: String) -> Commands {
	let mut commands: Commands = Vec::new();
	for line in text.split("\n") {
		if line.chars().count() == 0 { continue; }
		let mut text = line.to_string();
		match text.remove(0) {
			// Comment
			'#' => {}
			// GoTo
			'@' => {
				text.remove(0);
				let mut line_num = String::new();
				while text.len() >= 1 {
					let x = text.remove(0);
					if !x.is_digit(10) { break; }
					line_num.push( x );
				}
				commands.push(Command::GoTo(line_num));
			},
			// Move Up
			'^' => {
				text.remove(0);
				let mut num = String::new();
				while text.len() >= 1 {
					let x = text.remove(0);
					num.push( x );
				}
				commands.push(Command::MoveUp(
					num.parse::<usize>().unwrap()
				));
			},
			// Move Down
			'v' => {
				text.remove(0);
				let mut num = String::new();
				while text.len() >= 1 {
					let x = text.remove(0);
					num.push( x );
				}
				commands.push(Command::MoveDown(
					num.parse::<usize>().unwrap()
				));
			},
            // Move left 
            '>' => {
                text.remove(0);
                let mut count = String::new();
                while text.len() >= 1 {
                    let x = text.remove(0);
                    count.push(x);
                }
                commands.push(Command::MoveLeft(
                    count.parse::<usize>().unwrap()
                ));
            },
            // Move right 
            '<' => {
                text.remove(0);
                let mut count = String::new();
                while text.len() >= 1 {
                    let x = text.remove(0);
                    count.push(x);
                }
                commands.push(Command::MoveRight(
                    count.parse::<usize>().unwrap()
                ));
            },
            // Find char right
            'f' => {
                text.remove(0);
                let c = text.remove(0);
                commands.push(Command::FindCharRight(c));
            },
            // Find char left
            'F' => {
                text.remove(0);
                let c = text.remove(0);
                commands.push(Command::FindCharRight(c));
            },
			// Add Line
			'+' => {
				if text.len() >= 1 { text.remove(0); }
				let mut new_line = String::new();
				while text.len() >= 1 {
					let x = text.remove(0);
					new_line.push( x );
				}
				commands.push(Command::AddLine(new_line));
			},
			// Write
			' ' => {
				if text.len() >= 1 { text.remove(0); }
				let mut write_text = String::new();
				while text.len() >= 1 {
					let x = text.remove(0);
					write_text.push( x );
				}
				commands.push(Command::Write(write_text));
			},
            // Yank Line 
            'y' => {
                commands.push(Command::YankLine); 
                // literally nothing has to be done after this
                // shocking, I know
            }, 
            // Pase line(s)
            'p' => {
                text.remove(0);
                let mut count = String::new();
                while text.len() >= 1 {
                    let x = text.remove(0);
                    count.push(x);
                }
                commands.push(Command::PaseLine(
                    count.parse::<usize>().unwrap() 
                    // @Todo replace unwrap's with expect's
                ));
            },
			// Prepend
			':' => {
				if text.len() >= 1 { text.remove(0); }
				let mut prepend_text = String::new();
				while text.len() >= 1 {
					let x = text.remove(0);
					prepend_text.push( x );
				}
				commands.push(Command::Prepend(prepend_text));
			},
			// Append
			';' => {
				if text.len() >= 1 { text.remove(0); }
				let mut append_text = String::new();
				while text.len() >= 1 {
					let x = text.remove(0);
					append_text.push( x );
				}
				commands.push(Command::Append(append_text));
			},
			// Remove Lines
			'-' => {
				text.remove(0);
				let mut remove_count = String::new();
				while text.len() >= 1 {
					let x = text.remove(0);
					if !x.is_digit(10) { break; }
					remove_count.push( x );
				}
				commands.push(Command::RemoveLines(
					remove_count.parse::<usize>().unwrap()
				));
			},
            // Delete inner 
            'd' => {
                text.remove(0);
                let c = text.remove(0);
                commands.push(Command::DeleteInner(c));
            },
			// Sleep Ms
			'z' => {
				text.remove(0);
				let mut ms = String::new();
				while text.len() >= 1 {
					let x = text.remove(0);
					if !x.is_digit(10) { break; }
					ms.push( x );
				}
				commands.push(Command::SleepMs(
					ms.parse::<u64>().unwrap()
				));
			}
			c => todo!("Can't parse command {}", c),
		}
	}
	commands
}
