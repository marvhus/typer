pub mod parse;
pub mod typer;

use parse::parse;
use typer::{
    typer,
    KeybindPreset,
};
use std::vec::Vec;

fn read_file(path: &str) -> String {
	std::fs::read_to_string(path)
		.expect("Failed to read file")
} 

fn main() {
	let args: Vec<String> = std::env::args().collect();
	if args.len() < 3 {
		eprintln!("Not enough arguments. It should be `typer [file] [mode]`");
        eprintln!("Note: Currently the only modes supported are `emacs` and `vim`");
		std::process::exit(1);
	}
    let text = read_file(&args[1]);
    let mode = match args[2].as_str() {
        "emacs" => KeybindPreset::Emacs,
        "vim" => KeybindPreset::Vim,
        _ => {
            eprintln!("Note: Currently the only modes supported are `emacs` and `vim`");
            std::process::exit(1);
        }
    };
	let out = parse(text);
	typer(out, mode);
}
